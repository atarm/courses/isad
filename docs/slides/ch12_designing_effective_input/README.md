---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Designing Effective Input_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 设计有效的输入Designing Effective Input

---

## Contents

1. 表单（form）设计
1. 屏幕（display）和Web表单（form）设计
1. 网站（website）设计

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 表单设计Good Form Design

---

+ 表单用于收集输入信息
+ 表单是控制工作进程的重要手段

---

### 表单设计的原则

1. 满足目标
1. 容易填写
1. 保证输入准确
1. 有吸引力

---

### 满足目标

+ 创建表单的目的是：记录、处理、保存和获取所需的信息，进而达到业务目标

---

### 容易填写

1. 符合习惯、符合逻辑的填写顺序：从左到右、自上而下
1. 表单7元素：标题、ID、使用说明/填写说明、表单主体、汇总、签名和确认、备注
1. 合适的legend

---

![height:600](./resources/image/fig12_01.jpg)

---

![height:600](./resources/image/fig12_02.jpg)

---

### 保证输入准确

1. 扫描输入数据
1. 提交前检验输入数据
1. 提示输入数据（如：曾经输入的正确数据）
1. 封闭式的输入数据
1. 反馈错误输入数据

---

### 有吸引力

+ 美观的表单能吸引用户的注意力，并能帮助准确完成表单的输入
+ 简洁、布局、流向、字体、线条

---

### 良好的表单输入的特征

1. 有效
1. 准确
1. 易用
1. 简单
1. 一致
1. 有吸引力

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 屏幕和Web表单设计Good Display and Web Forms Design

---

### 保持屏幕简洁

+ 屏幕显示的三个部分
    1. 顶部：标题栏、菜单栏等
    1. 中部：数据输入，提供合适的标题和使用说明
    1. 底部：备注和指令

---

![height:610](./resources/image/fig12_03.jpg)

---

### 保持屏幕的一致性

+ 习惯一致性：
    1. 纸质表单与电子表单一致
    1. 各个表单风格、位置一致
+ 逻辑一致性/亲密性：相关的靠近、不相关的分开

---

### 方便用户在屏幕间移动

+ “三次单击”规则：用户应能在单击三次鼠标或按键之内，转到他们所需的屏幕
+ 实现屏幕的方法：
    1. 页面跳转
    1. 向下滚动
    1. 弹出窗口

---

### 有吸引力且令人愉快的屏幕

+ 作用：提高效率、减少犯错
+ 作法：
    1. 留白
    1. 合适的字体字号

---

<!--
_backgroundColor: Cornsilk
_color:
_class:
    - lead
-->

### 图标Icons

---

![width:1100](./resources/image/font-awesome.png)

---

![width:1100](./resources/image/google_material-design-icons.png)

---

![width:1100](./resources/image/google-icons.jpg)

---

<!--
_backgroundColor: Cornsilk
_color:
_class:
    - lead
-->

### HTML5表单控件

---

#### 容器类

1. `<form>`
1. `<fieldset>`
    1. `<legend>`

---

#### 显示类

1. `<label>`
1. `<progress>`
1. `<meter>`
1. `<output>`
1. `<datalist>`
    1. `<option>`

---

#### 输入类

1. `<input>`
1. `<textarea>`
1. `<button>`
1. `<select>`
    1. `<optgroup>`
        1. `<option>`

---

#### 输入类--input的type

1. `text`
1. `radio`
1. `checkbox`
1. `submit`
1. `button`
1. `hidden`

---

#### 输入类--input的type（HTML5新增）

1. `color`
1. `date`, `datetime`, `datetime-local`, `month`, `week`, `time`
1. `email`
1. `number`
1. `range`
1. `search`
1. `tel`
1. `url`

---

### 事件响应表

---

![height:610](./resources/image/fig12_06.jpg)

---

### 交互式网页

1. 网页端人机交互
1. 网页-服务交互

---

### 准确使用色彩搭配

1. “色不过三”：一个主色（60%）、一个辅色（30%）、一个点缀色（10%）
1. 总色相数不超过7

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 网站设计Website Design

---

### 常见网页元素

+ 汉堡图标hamburger icon/汉堡菜单hamburger menu
    1. 三个横条堆叠
    1. 通常放在显示的顶部，提示用户可以点击图标以显示菜单，特别适合应用于移动设备
+ 上下文相关帮助：鼠标悬停时光标变成问号，单击问号将显示特定帮助文档
+ 输入校验

---

+ 面包屑导航breadcrumb navigation/页面路径
+ 页脚
    1. 公司基本信息：联系信息、版权声明、网站内容联系人等
    1. 公司文化信息：组织声明、企业口号、关键价值等
    1. 社交媒体链接
    1. 关键字或标签

---

### Web设计的指导原则

1. 提供清楚的用户语言的用法说明
1. 提供表单填写顺序的说明
1. 使用合适的、多样化的表单控件
1. 必要时提供可滚动的文本框
1. 每个web表单提供`submit`和`clear`按键
1. 尝试将复杂的单个表单分割成多个简单的表单
1. 提供一个颜色区分明确的错误反馈

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
